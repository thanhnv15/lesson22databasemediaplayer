package com.nac.lesson22k4database;

import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.nac.lesson22k4database.db.BaiHatInfo;

import java.util.ArrayList;
import java.util.List;

public class BaiHatManager {
    public static final int KEY_GET_ALL_BH = 98;
    private static BaiHatManager instance;

    private BaiHatManager() {
        //for singleton
    }

    public static BaiHatManager getInstance() {
        if (instance == null) {
            instance = new BaiHatManager();
        }
        return instance;
    }

    public void getAllBH(OnActionCallBack callBack) {
        MTask task = new MTask(null, new MTask.OnMTaskCallBack() {
            @Override
            public Object execTask(String key, Object data, MTask task) {
                return App.getInstance().getDB().getBaiHatDAO().getListBH();
            }

            @Override
            public void completeTask(String key, Object data) {
                callBack.callBack(KEY_GET_ALL_BH, data);
            }
        });
        task.start(null);
    }

    public List<BaiHatInfo> getAllOfflineSong() {
        List<BaiHatInfo> baiHats = new ArrayList<>();
        try {
            Uri songDB = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            Cursor c = App.getInstance().getContentResolver()
                    .query(songDB, null,
                            null, null, null);
            if (c == null) {
                return baiHats;
            }
            c.moveToFirst();
            while (!c.isAfterLast()) {
                String id = c.getString(c.getColumnIndex(MediaStore.Audio.Media._ID));
                String chuDe = c.getString(c.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                String tenBH = c.getString(c.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String tenCasy = c.getString(c.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                String link = c.getString(c.getColumnIndex(MediaStore.Audio.Media.DATA));

                BaiHatInfo bh = new BaiHatInfo();
                bh.setMaBH(Integer.parseInt(id));
                bh.setTenCD(chuDe);
                bh.setCasy(tenCasy);
                bh.setTen(tenBH);
                bh.setLink(link);

                baiHats.add(bh);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return baiHats;
    }
}
