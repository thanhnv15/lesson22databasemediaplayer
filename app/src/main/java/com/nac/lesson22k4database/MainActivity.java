package com.nac.lesson22k4database;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nac.lesson22k4database.adapter.SongAdapter;
import com.nac.lesson22k4database.db.BaiHatInfo;

import java.util.List;

public class MainActivity extends AppCompatActivity
        implements OnActionCallBack, View.OnClickListener, MTask.OnMTaskCallBack, SeekBar.OnSeekBarChangeListener {

    private static final String TAG = MainActivity.class.getName();
    private static final int REQ_PERMISSION_READ_STORAGE = 101;
    private static final String KEY_UPDATE_TIME_SONG = "KEY_UPDATE_TIME_SONG";
    private RecyclerView rvSong;
    private ImageView ivPlay, ivBack, ivNext;
    private TextView tvSong, tvSinger, tvDuration;
    private MTask mTask;
    private SeekBar mSeekBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        MediaManager.getInstance().setCallBack(this);
        mTask = new MTask(KEY_UPDATE_TIME_SONG, this);
        mTask.start(null);

        ivBack = findViewById(R.id.iv_back);
        ivNext = findViewById(R.id.iv_next);
        ivPlay = findViewById(R.id.iv_play);

        tvSinger = findViewById(R.id.tv_singer);
        tvSong = findViewById(R.id.tv_song);
        tvDuration = findViewById(R.id.tv_duration);
        mSeekBar = findViewById(R.id.seek_bar);
        mSeekBar.setOnSeekBarChangeListener(this);

        ivPlay.setOnClickListener(this);
        ivNext.setOnClickListener(this);
        ivBack.setOnClickListener(this);

        rvSong = findViewById(R.id.rv_song);
        rvSong.setLayoutManager(new LinearLayoutManager(this));

//        BaiHatManager.getInstance().getAllBH((key, data) -> {
//            if (key == BaiHatManager.KEY_GET_ALL_BH) {
//                List<BaiHatInfo> listBH = (List<BaiHatInfo>) data;
//                MediaManager.getInstance().setListBH(listBH);
//
//                Log.i(TAG, "listBH: " + listBH.size());
//                SongAdapter adapter = new SongAdapter(listBH, MainActivity.this, MainActivity.this);
//                rvSong.setAdapter(adapter);
//                adapter.initFirst();
//            }
//        });

        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE
            }, REQ_PERMISSION_READ_STORAGE);
            return;
        }
        loadOfflineSong();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQ_PERMISSION_READ_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadOfflineSong();
            }
        }
    }

    private void loadOfflineSong() {
        List<BaiHatInfo> listBH = BaiHatManager.getInstance().getAllOfflineSong();
        MediaManager.getInstance().setListBH(listBH);
        Log.i(TAG, "listBH: " + listBH.size());
        SongAdapter adapter = new SongAdapter(listBH, MainActivity.this, MainActivity.this);
        rvSong.setAdapter(adapter);
        adapter.initFirst();
    }

    @Override
    public void callBack(int key, Object data) {
        if (key == MediaManager.KEY_STATE_PLAYER) {
            int state = (int) data;
            if (state == MediaManager.STATE_IDLE || state == MediaManager.STATE_PAUSED) {
                ivPlay.setImageLevel(0);
            } else {
                ivPlay.setImageLevel(1);
            }

            tvSong.setText(MediaManager.getInstance().getBH().getTen());
            tvSinger.setText(MediaManager.getInstance().getBH().getCasy());
            ((SongAdapter) rvSong.getAdapter())
                    .updateUI(MediaManager.getInstance().getBH());

            rvSong.scrollToPosition(MediaManager.getInstance().getIndex());
        } else if (key == SongAdapter.KEY_GET_ONE_BH) {
            BaiHatInfo baiHat = (BaiHatInfo) data;
            MediaManager.getInstance().play(baiHat);
        }
    }

    @Override
    public void onClick(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.anim_alpha));
        switch (view.getId()) {
            case R.id.iv_play:
                if (MediaManager.getInstance().isPlaying()) {
                    MediaManager.getInstance().pause();
                } else {
                    MediaManager.getInstance().play();
                }
                break;
            case R.id.iv_back:
                MediaManager.getInstance().previous();
                break;
            case R.id.iv_next:
                MediaManager.getInstance().next();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onStop() {
        MediaManager.getInstance().stop();
        super.onStop();
    }

    @Override
    public Object execTask(String key, Object data, MTask task) {
        while (true){
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }
            task.requestUpdate(null);
        }
    }

    @Override
    public void updateUI(String key, Object data) {
        try {
            if(MediaManager.getInstance().isPlaying()){
                String currentTime = MediaManager.getInstance().getCurrentTimeText();
                String totalTime = MediaManager.getInstance().getTotalTimeText();

                tvDuration.setText(String.format("%s/%s", currentTime, totalTime));
                mSeekBar.setMax(MediaManager.getInstance().getDuration());
                mSeekBar.setProgress(MediaManager.getInstance().getCurrentDuration());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        MediaManager.getInstance().seekTo(seekBar.getProgress());
    }
}
