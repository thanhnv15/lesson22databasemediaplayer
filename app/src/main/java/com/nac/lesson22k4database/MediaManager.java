package com.nac.lesson22k4database;

import android.media.MediaPlayer;

import com.nac.lesson22k4database.db.BaiHatInfo;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MediaManager {
    public static final int STATE_IDLE = 0;
    public static final int STATE_PLAYING = 1;
    public static final int STATE_PAUSED = 2;
    public static final int KEY_STATE_PLAYER = 105;

    private static MediaManager instance;
    private List<BaiHatInfo> mListBH;
    private MediaPlayer player;
    private int index;
    private int state;
    private OnActionCallBack callBack;

    private MediaManager() {
        player = new MediaPlayer();
        player.setOnCompletionListener(mediaPlayer -> next());
        index = 0;
        state = STATE_IDLE;
    }

    public static MediaManager getInstance() {
        if (instance == null) {
            instance = new MediaManager();
        }
        return instance;
    }

    public void setCallBack(OnActionCallBack callBack) {
        this.callBack = callBack;
    }

    public void setListBH(List<BaiHatInfo> listBH) {
        mListBH = listBH;
    }

    public void play() {
        try {
            if (state == STATE_PAUSED) {
                player.start();
                state = STATE_PLAYING;
            } else if (state == STATE_IDLE) {
                player.reset();
                player.setDataSource(mListBH.get(index).getLink());
                player.prepare();

                player.start();
                state = STATE_PLAYING;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        callBack.callBack(KEY_STATE_PLAYER, state);
    }

    public void pause() {
        if (state == STATE_PLAYING && player.isPlaying()) {
            player.pause();
            state = STATE_PAUSED;
        }
        callBack.callBack(KEY_STATE_PLAYER, state);
    }

    public void next() {
        index++;
        if (index > mListBH.size()) {
            index = 0;
        }
        state = STATE_IDLE;
        play();
    }

    public void previous() {
        index--;
        if (index <= 0) {
            index = mListBH.size() - 1;
        }
        state = STATE_IDLE;
        play();
    }

    public boolean isPlaying() {
        return state == STATE_PLAYING && player.isPlaying();
    }

    public void play(BaiHatInfo baiHat) {
        index = mListBH.indexOf(baiHat);
        state = STATE_IDLE;
        play();
    }

    public BaiHatInfo getBH() {
        return mListBH.get(index);
    }

    public int getIndex() {
        return index;
    }

    public void stop() {
        if (player.isPlaying()) {
            pause();
        } else {
            state = STATE_IDLE;
            player.reset();
        }
    }

    public String getCurrentTimeText() {
        int time = player.getCurrentPosition();
        SimpleDateFormat df = new SimpleDateFormat("mm:ss");
        return df.format(new Date(time));
    }

    public String getTotalTimeText() {
        int time = player.getDuration();
        SimpleDateFormat df = new SimpleDateFormat("mm:ss");
        return df.format(new Date(time));
    }

    public int getDuration() {
        return player.getDuration();
    }

    public int getCurrentDuration() {
        return player.getCurrentPosition();
    }

    public void seekTo(int progress) {
        player.seekTo(progress);
        callBack.callBack(KEY_STATE_PLAYER, state);
    }
}
