package com.nac.lesson22k4database.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {BaiHat.class, ChuDe.class}, version = 1)
public abstract class AppDB extends RoomDatabase {
    public abstract BaiHatDAO getBaiHatDAO();
}
